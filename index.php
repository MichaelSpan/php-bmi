<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>BMI-Rechner</title>

    <script type="text/javascript" src="js/Validate.js"></script>

</head>
<body>
<div class="container">
<h1>BMI-Rechner</h1>

    <?php

    require "lib/func.php";

    $name = "";
    $messdatum = "";
    $groesse = "";
    $gewicht = "";
    $bmi = "";

    if (isset($_POST["submit"]))
    {
        $name = isset($_POST["name"]) ? $_POST["name"] : "";
        $messdatum = isset($_POST["messdatum"]) ? $_POST["messdatum"] : "";
        $groesse = isset($_POST["groesse"]) ? $_POST["groesse"] : "";
        $gewicht = isset($_POST["gewicht"]) ? $_POST["gewicht"] : "";

        if (validate($name, $messdatum, $groesse, $gewicht)) {
            echo "<p class='alert alert-success close'>Die eingegebenen Daten sind in Ordnung</p>";

            try
            {
                if (berechneBMI($groesse, $gewicht) < 18.5)
                {
                    echo "<p class='alert alert-warning'>$bmi Sie haben Untergewicht!</p>";
                } else if (berechneBMI($groesse, $gewicht) < 25.0 && berechneBMI($groesse, $gewicht) >= 18.5)
                {
                    echo "<p class='alert alert-success'>$bmi Sie haben Normalgewicht!</p>";
                } else if (berechneBMI($groesse, $gewicht) < 30.0 && berechneBMI($groesse, $gewicht) >= 25.0)
                {
                    echo "<p class='alert alert-warning'>$bmi Sie haben Übergewicht!</p>";
                } else if (berechneBMI($groesse, $gewicht) >= 30.0) {
                    echo "<p class='alert alert-danger'>$bmi Achtung! Adipositas!</p>";
                }
            } catch (Exception $e) {
                $errors["bmi"] = "Der BMI ist ungültig!";
            }
        } else {
            echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft</p></ul>";

            foreach ($errors as $key => $value) {
                echo "<li>" . $value . "</li>";
            }
            echo "</ul></div>";
        }
    }

    ?>

<form id="form_grade" action="index.php" method="post">

    <div class="row">

        <div class="col-sm-8">

            <div class="row">

                <div class="col-sm-8">
                    <label for="name">Name*</label>
                    <input type="text" id="name" name="name" class="form-control" <?= isset($errors["name"]) ? "is-invalid" : ""?> value="<?= htmlspecialchars($name) ?>" maxlength="25" required/>
                </div>

                <div class="col-sm-4">
                    <label for="messdatum">Messdatum*</label>
                    <input type="date" id="messdatum" name="messdatum" class="form-control" <?= isset($errors["messdatum"]) ? "is-invalid" : ""?> onchange="validiereMessdatum(this)" value="<?= htmlspecialchars($messdatum) ?>" required/>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-6">
                    <label for="groesse">Größe (cm)*</label>
                    <input type="number" id="groesse" name="groesse" class="form-control" <?= isset($errors["groesse"]) ? "is-invalid" : ""?> value="<?= htmlspecialchars($groesse) ?>" max="300" min="1" required/>
                </div>

                <div class="col-sm-6">
                    <label for="gewicht">Gewicht (kg)*</label>
                    <input type="number" id="gewicht" name="gewicht" class="form-control" <?= isset($errors["gewicht"]) ? "is-invalid" : ""?> value="<?= htmlspecialchars($gewicht) ?>" max="500" min="1" required/>
                </div>

            </div>

        </div>

        <div class="col-sm-4">

            <div class="col-sm-12">
                <h1>Info zum BMI</h1>
                <p>Unter 18.5 Untergewicht<br>18.5 - 24.9 Normal<br>25.0 - 29.9 Übergewicht<br>30.0 und darüber Adipositas</p>
            </div>

        </div>

    </div>

    <div class="row mt-3">

        <div class="col-sm-3 mb-3">

            <input type="submit" name="submit" class="btn btn-primary btn-block" value="Speichern"/>

        </div>

        <div class="col-sm-3">

            <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>

        </div>

    </div>

</form>
</div>
</body>
</html>