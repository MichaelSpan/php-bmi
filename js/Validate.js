function validiereMessdatum(elem)
{
    let today = new Date();
    today.setHours(0,0,0,0);

    let messdatum = new Date(elem.value);
    messdatum.setHours(0,0,0,0);

    if (messdatum <= today)
    {
        elem.classList.add("is-valid");
        elem.classList.remove("is-invalid");
    }else
    {
        elem.classList.add("is-invalid");
        elem.classList.remove("is-valid");
    }

}