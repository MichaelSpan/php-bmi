<?php

$errors = [];
$bmi = [];

function berechneBMI($groesse, $gewicht)
{
    global $bmi;

    $bmi = $gewicht / pow($groesse / 100, 2);
    $bmi = round($bmi, 1,PHP_ROUND_HALF_UP);

    return $bmi;
}

function validate($name, $messdatum, $groesse, $gewicht)
{
    return validateName($name) & validateMessdatum($messdatum) & validateGewicht($gewicht) & validateGroesse($groesse);
}

function validateName($name)
{
    global $errors;

    if ((strlen($name) == 0)) {
        $errors["name"] = "Name darf nicht leer sein.";
        return false;
    } else if (strlen($name) > 20) {
        $errors["name"] = "Name darf nicht länger als 25 Zeichen sein.";
        return false;
    } else {
        return true;
    }
}

function validateMessdatum($messdatum)
{
    global $errors;

    try
    {
        if ($messdatum == "") {
            $errors["messdatum"] = "Datum darf nicht leer sein.";
            return false;
        } else if (new DateTime($messdatum) > new DateTime()) {
            $errors["messdatum"] = "Datum darf nicht in der Zukunft liegen.";
            return false;
        } else {
            return true;
        }
    }catch (Exception $exception)
    {
        $errors["messdatum"] = "Messdatum ungültig";
        return false;
    }
}

function validateGewicht($gewicht)
{
    global $errors;

    if (!is_numeric($gewicht) || $gewicht < 1 || $gewicht > 500) {
        $errors["gewicht"] = "Gewicht ungültig.";
        return false;
    } else {
        return true;
    }
}

function validateGroesse($groesse)
{
    global $errors;

    if (!is_numeric($groesse) || $groesse < 1 || $groesse > 300) {
        $errors["groesse"] = "Größe ungültig.";
        return false;
    } else {
        return true;
    }
}